﻿using CsvHelper;
using CsvHelper.Configuration;
using MailSenderWithTests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
namespace MailSenderWithTests.DataParser.Tests
{
    namespace MailSenderWithTests.SenderTests
    {
        [TestClass]
        public class FileParserTests
        {
            [TestMethod]
            public void CheckEmailValueFromFileTest()
            {
                var delimeter = ";";
                Person toTest;
                var testResult = "aaad@30wave.com";
                using (var stream = new MemoryStream())
                using (var streamReader = new StreamReader(stream))
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write("Imie1;Nazwisko1;aaad@30wave.com");
                    streamWriter.Flush();
                    stream.Position = 0;
                    var csv = new CsvReader(streamReader, new CsvConfiguration
                    {
                        HasHeaderRecord = false,
                        Delimiter = delimeter
                    });
                    csv.Read();
                    toTest = csv.GetRecord<Person>();
                }
                Assert.AreEqual(toTest.Email, testResult);
            }

            [TestMethod]
            public void CheckNameValueFromFileTest()
            {
                var delimeter = ";";
                Person toTest;
                var testResult = "Imie1";
                using (var stream = new MemoryStream())
                using (var streamReader = new StreamReader(stream))
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write("Imie1;Nazwisko1;aaad@30wave.com");
                    streamWriter.Flush();
                    stream.Position = 0;
                    var csv = new CsvReader(streamReader, new CsvConfiguration
                    {
                        HasHeaderRecord = false,
                        Delimiter = delimeter
                    });
                    csv.Read();
                    toTest = csv.GetRecord<Person>();
                }
                Assert.AreEqual(toTest.Name, testResult);
            }

            [TestMethod]
            public void CheckSecondNameValueFromFileTest()
            {
                var delimeter = ";";
                Person toTest;
                var testResult = "Nazwisko1";
                using (var stream = new MemoryStream())
                using (var streamReader = new StreamReader(stream))
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write("Imie1;Nazwisko1;aaad@30wave.com");
                    streamWriter.Flush();
                    stream.Position = 0;
                    var csv = new CsvReader(streamReader, new CsvConfiguration
                    {
                        HasHeaderRecord = false,
                        Delimiter = delimeter
                    });
                    csv.Read();
                    toTest = csv.GetRecord<Person>();
                }
                Assert.AreEqual(toTest.SecondName, testResult);
            }
        }
    }
}
