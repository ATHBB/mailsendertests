﻿using MailSenderWithTests.DataParser.ApplicationIteration;
using MailSenderWithTests.DataParser.Parser;
using MailSenderWithTests.Logger;
using MailSenderWithTests.Sender;
using Ninject.Modules;

namespace MailSenderWithTests.Depadency
{
    public class MailSenderModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger.Logger>().InSingletonScope();
            Bind<IIterationValue>().To<IterationValue>().InSingletonScope();
            Bind<IFileReader>().To<FileReader>().InSingletonScope();
            Bind<IMailSender>().To<MailSender>().InSingletonScope();
        }
    }
}
