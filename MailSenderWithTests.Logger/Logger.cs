﻿using MailSenderWithTests.Data.Application;
using Serilog;
using System;

namespace MailSenderWithTests.Logger
{
    public class Logger : ILogger
    {
        public Logger()
        {
            InitializeLogger();
        }

        public void InitializeLogger()
        {
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile($"log-{DateTime.Now}.txt")
                    .CreateLogger();
        }

        public void AddLogEntry(string message)
        {
            if (string.IsNullOrWhiteSpace(message)) throw new Exception(ExceptionTexts.emptyMessageToLog);
            Log.Information(message);
        }

        public void AddLogError(string message, Exception ex)
        {
            if (string.IsNullOrWhiteSpace(message)) throw new Exception(ExceptionTexts.emptyMessageToLog);
            if (ex == null) throw new Exception(ExceptionTexts.exceptionToLogIsNull);
            Log.Error(ex, message);
        }
    }
}
