﻿using MailSenderWithTests.Data.Types;
using System.Collections.Generic;

namespace MailSenderWithTests.DataParser
{
    public interface IFileReader
    {
        List<Person> GetDataFromFileFile(string filePath, string delimeter, int iteration);
    }
}