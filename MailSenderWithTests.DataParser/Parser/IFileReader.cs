﻿using MailSenderWithTests.Data.Types;
using System.Collections.Generic;

namespace MailSenderWithTests.DataParser.Parser
{
    public interface IFileReader
    {
        List<Person> GetDataFromFileFile(string filePath, string delimeter, int iteration);
    }
}