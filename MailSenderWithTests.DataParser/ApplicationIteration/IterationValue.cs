﻿using System;
using System.Configuration;

namespace MailSenderWithTests.DataParser.ApplicationIteration
{
    public class IterationValue : IIterationValue
    {
        public int? GetIterationValue()
        {
            int Iteration;
            if (int.TryParse(ConfigurationManager.AppSettings[nameof(Iteration)], out Iteration))
                return Iteration;
            return null;
        }

        public void IncrementIterationValue()
        {
            int Iteration;
            if (int.TryParse(ConfigurationManager.AppSettings[nameof(Iteration)], out Iteration) == false) throw new Exception();
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            Iteration++;
            configuration.AppSettings.Settings[nameof(Iteration)].Value = Iteration.ToString();
            configuration.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
