﻿namespace MailSenderWithTests.Data.Application
{
    public static class ProgramInfo
    {
        static int itertaion;
        public static int Iteration { get { return itertaion; } }
        public static string MailBodyTemplateName { get; set; }
        public static string DataFilePath { get; set; }
        public static string Delimeter { get; set; }
        public static string ServiceName { get; set; }
        public static void IncrementIteration()
        {
            itertaion++;
        }
    }
}
