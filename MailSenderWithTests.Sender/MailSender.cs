﻿using FluentMailer.Factory;
using FluentMailer.Interfaces;
using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.Logger;
using System;

namespace MailSenderWithTests.Sender
{
    public class MailSender : IMailSender
    {
        readonly IFluentMailer _FluentMailer;
        readonly ILogger _Logger;

        public MailSender(ILogger Logger)
        {
            _Logger = Logger;
            _FluentMailer = FluentMailerFactory.Create();
        }

        public void SendMail(MailToSend mailToSend, string templateFileName)
        {
            if (string.IsNullOrWhiteSpace(mailToSend.MailData.Email) || string.IsNullOrWhiteSpace(mailToSend.MailData.Name) || string.IsNullOrWhiteSpace(mailToSend.MailData.SecondName)) throw new Exception(ExceptionTexts.dataToSendIsInvalid);
            if (mailToSend.VerifyEmail() == false) throw new Exception(ExceptionTexts.invalidEmailValue);
            if (mailToSend.ErrorCounter < 0 || mailToSend.ErrorCounter > 5) throw new ArgumentOutOfRangeException(nameof(mailToSend.ErrorCounter), ExceptionTexts.invalidErrorCounterValue);
            try
            {
                _Logger.AddLogEntry($"Rozpoczynam wysyłanie maila do: {mailToSend.MailData.Name}");
                _FluentMailer.CreateMessage()
                       .WithView(templateFileName, mailToSend.MailData)
                       .WithReceiver(mailToSend.MailData.Email)
                       .WithSubject("Zostań konsultantem AVON!!")
                       .Send();
                mailToSend.AfterSuccessfullySend();
            }
            catch (Exception ex)
            {
                _Logger.AddLogError(ExceptionTexts.sendMailErrorMessage, ex);
                mailToSend.AfterErrorSend();
                while (mailToSend.ErrorCounter < 5)
                    SendMail(mailToSend, templateFileName);
            }
        }


    }
}
