﻿using MailSenderWithTests.Data.Types;

namespace MailSenderWithTests.Sender
{
    public interface IMailSender
    {
        void SendMail(MailToSend mailToSend, string templateFilePath);
    }
}