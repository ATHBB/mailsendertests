﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.Sender;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MailSenderWithTests.UnitTests
{
    public class MailSenderTests
    {
        public static object[] Data()
        {
            return new object[]
                {
                    new object[] { new MailToSend(new Person()), null, ExceptionTexts.dataToSendIsInvalid},
                    new object[] { new MailToSend(new Person { Email = "email", Name = "name", SecondName = "secondName" }), null , ExceptionTexts.invalidEmailValue }
                };
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void SendMailInvalidDataToSendTest(MailToSend mailToSend, string templateFileName, string exceptionText)
        {
            var mailSender = new MailSender(new Logger.Logger());
            var ex = Assert.Throws<Exception>(() => mailSender.SendMail(mailToSend, templateFileName));
            Assert.Contains(exceptionText, ex.Message);
        }

        [Fact]
        public void SendMailErrorCounterValueTest()
        {
            var mailToSend = new MailToSend(new Person { Email = "email@test.com", Name = "name", SecondName = "secondName" });
            var mailSender = new MailSender(new Logger.Logger());
            for (int i = 0; i <= 5; i++)
            {
                mailToSend.AfterErrorSend();
            }
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => mailSender.SendMail(mailToSend, null));
            Assert.Contains(ExceptionTexts.invalidErrorCounterValue, ex.Message);
        }

        [Fact]
        public void SendMail()
        {
            var testDirectoryPath = ConfigurationManager.AppSettings["directoryFilePathToTestMails"];
            var mailTemplatePath = @"TestData\MailTemplate.cshtml";
            if (string.IsNullOrWhiteSpace(testDirectoryPath)) throw new Exception("Config value for test file path is null or empty");
            if (Directory.Exists(testDirectoryPath))
                Array.ForEach(Directory.GetFiles(testDirectoryPath), File.Delete);
            else
                Directory.CreateDirectory(testDirectoryPath);
            var mailToSend = new MailToSend(new Person { Email = "email@test.com", Name = "name", SecondName = "secondName" });
            var mailSender = new MailSender(new Logger.Logger());
            mailSender.SendMail(mailToSend, mailTemplatePath);
            Assert.True(Directory.GetFiles(testDirectoryPath).Any());
        }
    }
}
