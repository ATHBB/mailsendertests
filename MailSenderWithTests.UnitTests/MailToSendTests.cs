﻿using MailSenderWithTests.Data.Types;
using Xunit;

namespace MailSenderWithTests.UnitTests
{
    public class MailToSendTests
    {
        [Fact]
        public void AfterSuccessfullySendTest()
        {
            var expected = true;
            var testObject = new MailToSend(new Person());
            testObject.AfterSuccessfullySend();
            Assert.Equal(expected, testObject.WasSend);
        }

        [Fact]
        public void AfterErrorSendCheckWasSendValueTest()
        {
            var expected = false;
            var testObject = new MailToSend(new Person());
            testObject.AfterErrorSend();
            Assert.Equal(expected, testObject.WasSend);
        }

        [Fact]
        public void AfterErrorSendCheckErrorCounterValueTest()
        {
            var expected = 1;
            var testObject = new MailToSend(new Person());
            testObject.AfterErrorSend();
            Assert.Equal(expected, testObject.ErrorCounter);
        }
    }
}
