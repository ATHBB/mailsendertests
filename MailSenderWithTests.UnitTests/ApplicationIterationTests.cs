﻿using MailSenderWithTests.DataParser.ApplicationIteration;
using System.Configuration;
using Xunit;

namespace MailSenderWithTests.UnitTests
{
    public class ApplicationIterationTests
    {
        const int Iteration = 2;
        public ApplicationIterationTests()
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[nameof(Iteration)].Value = Iteration.ToString();
            configuration.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }

        [Fact]
        public void GetIterationTest()
        {
            var _iteration = new IterationValue();
            var expected = Iteration;
            Assert.Equal(expected, _iteration.GetIterationValue().Value);
        }

        [Fact]
        public void IncrementIterationTest()
        {
            var _iteration = new IterationValue();
            var expected = Iteration + 1;
            _iteration.IncrementIterationValue();
            Assert.Equal(expected, _iteration.GetIterationValue().Value);
        }
    }
}
