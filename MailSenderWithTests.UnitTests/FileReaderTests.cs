﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.DataParser.Parser;
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace MailSenderWithTests.UnitTests
{
    public class FileReaderTests
    {
        public static object[] Data()
        {
            return new object[]
                {
                    new object[] { null, null,0,ExceptionTexts.dataFileValueIsNullOrWhitespace},
                    new object[] { @"abcd:\myfile", ";", 0 ,ExceptionTexts.dataFileDoesntExist},
                    new object[] { @"abcd:\myfile", null, 0 ,ExceptionTexts.delimeterValueIsNull}
                };
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void GetDataFromFileDataExceptionTest(string filePath, string delimeter, int iteration, string exceptionText)
        {
            var fileReader = new FileReader();
            var expectedEx = Assert.Throws<Exception>(() => fileReader.GetDataFromFileFile(filePath, delimeter, iteration));
            Assert.Contains(exceptionText, expectedEx.Message);
        }

        [Fact]
        public void GetDataFromFileDataInvalidOperationExceptionTest()
        {
            var fileReader = new FileReader();
            var expectedEx = Assert.Throws<ArgumentOutOfRangeException>(() => fileReader.GetDataFromFileFile(@"abcd:\myfile", ";", -1));
            Assert.Contains(ExceptionTexts.invalidIterationValue, expectedEx.Message);
        }

        [Fact]
        public void GetDataFromFileTest()
        {
            var fileReader = new FileReader();
            var expected = new Person { Email = "aaad@30wave.com", Name = "Imie1", SecondName = "Nazwisko1" };
            var objectToTest = fileReader.GetDataFromFileFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"TestData\testData.csv"), ";", 0);
            Assert.Equal(expected.Email, objectToTest.FirstOrDefault().Email);
            Assert.Equal(expected.Name, objectToTest.FirstOrDefault().Name);
            Assert.Equal(expected.SecondName, objectToTest.FirstOrDefault().SecondName);
        }
    }
}
