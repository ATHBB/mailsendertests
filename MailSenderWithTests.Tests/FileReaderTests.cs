﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.DataParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace MailSenderWithTests.Tests
{
    [TestClass]
    public class FileReaderTests
    {
        [TestMethod]
        public void GetDataFromFileDataFileValueTest()
        {
            var fileReader = new FileReader();
            try
            {
                fileReader.GetDataFromFileFile(null, null, 0);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.dataFileValueIsNullOrWhitespace);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void GetDataFromFileDataFileDoesntExistTest()
        {
            var fileReader = new FileReader();
            try
            {
                fileReader.GetDataFromFileFile(@"abcd:\myfile", ";", 0);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.dataFileDoesntExist);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void GetDataFromFileDelimeterValueTest()
        {
            var fileReader = new FileReader();
            try
            {
                fileReader.GetDataFromFileFile(@"abcd:\myfile", null, 0);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.delimeterValueIsNull);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void GetDataFromFileIterationValueTest()
        {
            var fileReader = new FileReader();
            try
            {
                fileReader.GetDataFromFileFile(@"abcd:\myfile", ";", -1);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.invalidIterationValue);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void GetDataFromFileTest()
        {
            var fileReader = new FileReader();
            var expected = new Person { Email = "aaad@30wave.com", Name = "Imie1", SecondName = "Nazwisko1" };
            var objectToTest = fileReader.GetDataFromFileFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"TestData\testData.csv"), ";", 0);
            Assert.AreEqual(expected.Email, objectToTest.FirstOrDefault().Email);
            Assert.AreEqual(expected.Name, objectToTest.FirstOrDefault().Name);
            Assert.AreEqual(expected.SecondName, objectToTest.FirstOrDefault().SecondName);
        }
    }
}
