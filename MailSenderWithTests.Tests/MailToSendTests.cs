﻿using MailSenderWithTests.Data.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MailSenderWithTests.Tests
{
    [TestClass]
    public class MailToSendTests
    {
        [TestMethod]
        public void AfterSuccessfullySendTest()
        {
            var expected = true;
            var testObject = new MailToSend(new Person());
            testObject.AfterSuccessfullySend();
            Assert.AreEqual(expected, testObject.WasSend);
        }

        [TestMethod]
        public void AfterErrorSendCheckWasSendValueTest()
        {
            var expected = false;
            var testObject = new MailToSend(new Person());
            testObject.AfterErrorSend();
            Assert.AreEqual(expected, testObject.WasSend);
        }

        [TestMethod]
        public void AfterErrorSendCheckErrorCounterValueTest()
        {
            var expected = 1;
            var testObject = new MailToSend(new Person());
            testObject.AfterErrorSend();
            Assert.AreEqual(expected, testObject.ErrorCounter);
        }
    }
}
